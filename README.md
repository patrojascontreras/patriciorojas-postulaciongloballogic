# PatricioRojas-PostulacionGlobalLogic

## Descripción

Proyecto en Java 11 fabricado para el consumo de servicios REST mediante el uso de tecnologías basadas en Spring Framework 5 a traves de Spring Boot en conjunto con Hibernate y JPA (Java Persistence API), que cumple con la funcionalidad de procesar a traves de operaciones CRUD (Lo cuál significa Create, Read, Update and Delete, estas son las cuatro operaciones básicas que se realizan en el almacenamiento persistente), que el cuál se ha realizado como postulación para la empresa multinacional GlobalLogic con el cliente Banco BCI.

Entonces, las operaciones CRUD estándar son las siguientes:

* **POST :** Crea un nuevo recurso.
* **GET :** Lee/recupera un recurso.
* **PUT :** Actualiza un recurso existente.
* **DELETE :** Elimina un recurso.

### Diagrama de arquitectura de alto nivel del proyecto:

![Diagrama de Arquitectura](/Imgs/img_diagrama_arquitectura_proyecto.png)

El diagrama de arquitectura de alto nivel representa el flujo de datos y las interacciones dentro de la aplicación Spring Boot. Ilustra cómo la interfaz de usuario/Postman se comunica con la API Spring Boot, que se coordina aún más con la capa de servicio y Spring Data JPA para realizar operaciones CRUD en la base de datos.

### Estructura del proyecto:

![Estructura del proyecto](/Imgs/estructura_proyecto.jpg)

Uso de Spring Security y JSON Web Token (JWT) para acceder a API's mediante autenticación de Usuario generando un token de acceso para poder determinar si un cliente tiene autoridad, o autorización, para acceder a ciertos recursos protegidos.

Conceptos básicos vinculados a autenticación en proyectos Java:

* **Spring Security :** Marco que se encarga de proporcionar autenticación, autorización y protección contra ataques comunes. Con soporte de primera clase para proteger aplicaciones imperativas y reactivas, es el estándar de facto para proteger aplicaciones basadas en Spring.
* **JSON Web Token  :** Estándar de código abierto basado en JSON para crear tokens de acceso que nos permiten securizar las comunicaciones entre cliente y servidor.

El proyecto contiene Pruebas unitarias que están fabricadas mediante Spring JUnit 5 en conjunto con Mockito, los cuales son frameworks vinculados a las pruebas de tests unitarios.

Uso de **Patrones de Diseño** en el proyecto:

* **Adapter    :** Es un patrón de tipo estructural que cumple con la funcionalidad de permitir la colaboración entre objetos con interfaces incompatibles. 
    
	En Spring Boot, los adaptadores se pueden implementar como controladores para adaptar las llamadas HTTP a la lógica de negocio. 
	![Ejemplo Adapter](/Imgs/SpringBootProject-DesignPattern-AdapterExample.jpg) 
	
* **Repository :** Es un patrón de diseño que se utiliza para separar la lógica de acceso a datos de la lógica de negocio en una aplicación. 

    Dentro de Spring Boot, los repositorios se pueden implementar fácilmente usando Spring Data JPA. 
	
	Definiendo una interfaz que extiende JpaRepository, Spring generará automáticamente la implementación en tiempo de ejecución. 
	![Ejemplo Repository](/Imgs/SpringBootProject-DesignPattern-RepositoryExample.jpg) 
	
* **Singleton :** Es un patrón de diseño creacional que garantiza que tan solo exista un objeto de su tipo y proporciona un único punto de acceso a él para cualquier otro código. 
    
	![Ejemplo Singleton a nivel general](/Imgs/singleton_example_class.jpg) 
	
	Dentro de Java y todo lo relacionado a Spring, este tipo de patrón suele utilizarse en clase que están relacionadas a temas de configuración de parametros generales de la aplicación ya que una vez instanciado el objeto los valores se mantienen y son compartidos por toda la aplicación. **Ejemplo:** Una clase de Conexión a una base de datos determinada. 
	
	Dentro de este proyecto, el patrón Singleton se está utilizando en una clase que está vinculada a una configuración determinada que aparece en la siguiente imagen: 
	![Ejemplo Singleton](/Imgs/SpringBootProject-DesignPattern-SingletonExample.jpg) 

Uso de **Arquitectura Limpia** (En inglés: **Clean Architecture**) mediante el estilo de arquitectura llamado **Organización de Código** (En inglés: **Organizing Code**) denonimado **En capas** (En ingles: **Layered**):  

Esta es la forma más sencilla de organizar paquetes, ya que cada paquete se define por su responsabilidad. Sin embargo, puede que no sea el mejor enfoque para organizar proyectos complejos. No obstante, se utiliza habitualmente para proyectos pequeños y la validación debe aplicarse en cada capa. 

Para llevarse a cabo esta forma de organizar código dentro de un proyecto, se puede organizar de la siguiente manera: 

![Imagen Layered - Arquitectura Limpia](/Imgs/clean-architecture_organization-code_layered_example.jpg) 

Dentro del proyecto, tambien se están utilizando uno de los siguientes **principios S.O.L.I.D**:
* **D - Dependency Inversion Principle (Principio de Inversión de Dependencia)      :** Es un principio que se encarga de establecer que una clase no debe depender de clases concretas, sino de abstracciones. En otras palabras, no debemos crear dependencias sobre clases específicas, sino sobre interfaces o clases abstractas. 
    
    Por ejemplo, digamos que tenemos una clase que depende de una clase de base de datos concreta. Si necesitamos cambiar la base de datos, también necesitaremos cambiar el código de la clase. Esto viola el principio de inversión de dependencia. 
    
    Una mejor manera de diseñar esto sería crear una dependencia en una clase de base de datos abstracta. Entonces podemos crear una clase MysqlDatabase concreta que amplíe la base de datos. De esta forma, podemos cambiar la base de datos sin tener que cambiar el código de la clase. 
	
	**Ejemplos DIP con Spring Boot:** 
    ![Principio Inversión de Dependencia - Ejemplo Incorrecto](/Imgs/SOLID-principles_Dependency-Inversion-Principle_incorrect-example.jpg) 
    ![Principio Inversión de Dependencia - Ejemplo Correcto](/Imgs/SOLID-principles_Dependency-Inversion-Principle_correct-example.jpg) 

##### Definición de Principios S.O.L.I.D: 

Principios SOLID se define como un conjunto de principios que se utilizan en el desarrollo de software para hacer que los diseños sean más comprensibles, flexibles, escalables y resistentes a los cambios. 

Los Principios Solid indican cómo organizar funciones y estructuras de datos en componentes y cómo dichos componentes deben estar interconectados. 

Normalmente estos componentes suelen ser clases, aunque esto no implica que dichos principios sólo sean aplicables a ellas. Por el contrario, los principios SOLID se pueden aplicar a cada producto de software. 

SOLID es un acrónimo que representa cinco principios utilizados por ingenieros de software y desarrolladores. 

Los principios son los siguientes: 

![Imagen Resumen Principios S.O.L.I.D](/Imgs/principios-solid_resumen.jpg) 

En este proyecto, también se está utilizando **Arquitectura Monolítica**, la cuál se define como un modelo tradicional de un programa de software que se compila como una unidad unificada y que es autónoma e independiente de otras aplicaciones. 

![Imagen Definición Arquitectura Monolítica](/Imgs/img_arquitectura_monolitica.jpg) 

Proyecto de tipo Gradle.

Conexión a Base de Datos Relacional H2 en memoria.

![Conexión a Base de Datos mediante Browser](/Imgs/conexion-bd-h2-browser.jpg)

La consola H2 se abre usando la siguiente url mediante el Browser: http://localhost:8080/SpringBoot-PostulacionGlobalLogic/h2-console

#### Diagrama de Componentes

![Diagrama de Componentes](/Imgs/diagrama_componentes_proyecto_postulacion_globallogic.png)

#### Diagrama de Secuencia

![Diagrama de Secuencia](/Imgs/diagrama_secuencia_proyecto_postulacion_globallogic.png)

#### Modelo de la Base de Datos

![Modelo Base de Datos del Proyecto](/Imgs/bd-desafiotecnico-bci.jpg)

### Configuración de la Base de Datos en el proyecto:

![Configuración Base de Datos del Proyecto](/Imgs/archivo_properties_proyecto.jpg)

### Ejecución de la aplicación:

Desde el IDE que en el cuál se está desarrollando la aplicación seleccionando el metodo Main del mismo proyecto:

* Paso 01:

![Compilación 01](/Imgs/paso01_ejecucion_proyecto.jpg)

* Paso 02:

![Compilación 02](/Imgs/paso02_ejecucion_proyecto.jpg)

### Despliegue de EndPoints:

**Paso 01:** Creación de Usuario

![Creación de Usuario 01](/Imgs/usercreate-post_api-rest-project_postulacion-globallogic.jpg)

![Creación de Usuario 02](/Imgs/usercreate-exist-post_api-rest-project_postulacion-globallogic.jpg)

**Paso 02:** Inicio de sesión de Usuario mediante Login y Password

![Iniciar sesión](/Imgs/login-session-post_api-rest-project_postulacion-globallogic.jpg)

#### Ejecución de Token en EndPoints:

![Ejecución Token en EndPoints](/Imgs/token_api-rest-project_postulacion-globallogic.jpg)

**Aviso importante:** Con respecto al token, se debe usar el token que se ha generado desde el EndPoint de Creación Usuario.

##### Documentación de Endpoints:

Uso de documentación Swagger UI, ingresando a traves de la siguiente url: http://localhost:8080/SpringBoot-PostulacionGlobalLogic/swagger-ui/index.html

![Documentación de EndPoints mediante Swagger](/Imgs/documentacion-swagger-proyecto-postulacion_globallogic.jpg)

**Aviso importante:** Con respecto a las contraseñas, se probaron mediante las siguientes descripciones:

* J@va1234
* Cruz@dos17

Realizado por Ing. Patricio Rojas Contreras - 17 de febrero de 2024.
