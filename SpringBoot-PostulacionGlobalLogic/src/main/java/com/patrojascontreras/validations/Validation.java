package com.patrojascontreras.validations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validation {

    /**
     * Valida la forma de una dirección de correo
     * @param email cadena de texto con el email a validar
     * @return
     */
    public boolean validarEmail(String email) {
        Pattern pattern = Pattern.compile("^([0-9a-zA-Z]+[-._+&])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$");
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    /*
     * Validar Contraseña de 4 a 32 caracteres que requiere al menos 3 de 4 (mayúsculas
     * y letras minúsculas, números y caracteres especiales) y como máximo
     * 2 caracteres consecutivos iguales.
     */

    private static final String COMPLEX_PASSWORD_REGEX =
            "^(?:(?=.*\\d)(?=.*[A-Z])(?=.*[a-z])|" +
                    "(?=.*\\d)(?=.*[^A-Za-z0-9])(?=.*[a-z])|" +
                    "(?=.*[^A-Za-z0-9])(?=.*[A-Z])(?=.*[a-z])|" +
                    "(?=.*\\d)(?=.*[A-Z])(?=.*[^A-Za-z0-9]))(?!.*(.)\\1{2,})" +
                    "[A-Za-z0-9!~<>,;:_=?*+#.\"&§%°()\\|\\[\\]\\-\\$\\^\\@\\/]" +
                    "{8,32}$";

    public boolean validarPassword(String password) {
        Pattern pattern = Pattern.compile(COMPLEX_PASSWORD_REGEX);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
