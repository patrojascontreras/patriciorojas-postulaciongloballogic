package com.patrojascontreras;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.patrojascontreras.models.entities.Role;
import com.patrojascontreras.models.entities.Roles;
import com.patrojascontreras.models.repositories.RolesRepository;

@SpringBootApplication
@EnableJpaRepositories
public class SpringBootPostulacionGlobalLogicApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootPostulacionGlobalLogicApplication.class, args);
	}

	@Autowired
	private RolesRepository rolesRepository;

	@Bean
	CommandLineRunner runner() {
		return args -> {
			Role role01 = new Role();
			role01.setRoleName(Roles.ROLE_ADMIN);

			rolesRepository.save(role01);

			Role role02 = new Role();
			role02.setRoleName(Roles.ROLE_USER);

			rolesRepository.save(role02);
		};
	};

}
