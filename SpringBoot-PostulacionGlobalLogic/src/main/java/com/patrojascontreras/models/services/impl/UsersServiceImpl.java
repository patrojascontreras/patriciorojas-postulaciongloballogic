package com.patrojascontreras.models.services.impl;

import java.util.*;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.patrojascontreras.models.entities.User;
import com.patrojascontreras.models.repositories.UsersRepository;
import com.patrojascontreras.models.services.UsersService;

@Service
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    @Autowired
    public UsersServiceImpl(UsersRepository usersRepository) {
        this.usersRepository = usersRepository;
    }

    public User userCreate(User user) {
        return usersRepository.save(user);
    }

    public User userUpdate(User user) {
        return usersRepository.save(user);
    }

    public void deleteById(String idUser) {
        usersRepository.deleteById(idUser);
    }

    public List<User> usersListAll() {
        return usersRepository.findAll();
    }

    public List<User> usersListAllById(String id) {
        return usersRepository.findAllByIdUser(id);
    }

    @Transactional
    public User getUserById(String id) {
        return usersRepository.findByIdUser(id);
    }

    @Transactional
    public Optional<User> getUserByIdWithOptional(String id) {
        return usersRepository.findById(id);
    }

    public User existByLogin(String username) {
        return usersRepository.findByEmail(username);
    }

}
