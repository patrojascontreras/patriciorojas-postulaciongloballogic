package com.patrojascontreras.models.entities;

public enum Roles {
    ROLE_USER,
    ROLE_ADMIN
}
