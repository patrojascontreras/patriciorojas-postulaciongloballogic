package com.patrojascontreras.models.repositories.auths;

import java.util.*;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.patrojascontreras.models.entities.User;

@Repository
public interface LoginRepository extends JpaRepository<User, String> {

    public Optional<User> findByEmail(String login);
}
