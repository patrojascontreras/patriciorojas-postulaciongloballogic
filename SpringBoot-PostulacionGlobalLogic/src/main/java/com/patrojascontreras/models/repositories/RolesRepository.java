package com.patrojascontreras.models.repositories;

import java.util.*;

import com.patrojascontreras.models.entities.Role;
import com.patrojascontreras.models.entities.Roles;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface RolesRepository extends JpaRepository<Role, Integer> {

    public Optional<Role> findByRoleName(Roles role);
}
