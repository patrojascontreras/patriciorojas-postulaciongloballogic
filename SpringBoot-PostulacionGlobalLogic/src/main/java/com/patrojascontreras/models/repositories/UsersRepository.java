package com.patrojascontreras.models.repositories;

import java.util.*;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.patrojascontreras.models.entities.User;

@Repository
public interface UsersRepository extends JpaRepository<User, String> {

    public User findByEmail(String login);
    public User findByIdUser(String id);
    public List<User> findAllByIdUser(String idUser);
}
