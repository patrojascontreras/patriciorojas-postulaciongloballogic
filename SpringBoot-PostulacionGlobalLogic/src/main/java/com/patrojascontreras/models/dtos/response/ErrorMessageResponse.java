package com.patrojascontreras.models.dtos.response;

import java.time.LocalDateTime;

public class ErrorMessageResponse {

    private LocalDateTime timestamp;
    private Integer codigo;
    private String detail;

    public ErrorMessageResponse(LocalDateTime timestamp, Integer codigo, String detail) {
        this.timestamp = timestamp;
        this.codigo = codigo;
        this.detail = detail;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public String getDetail() {
        return detail;
    }

}
