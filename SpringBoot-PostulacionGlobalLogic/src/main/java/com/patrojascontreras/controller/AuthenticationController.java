package com.patrojascontreras.controller;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.patrojascontreras.config.security.jwts.JwtTokenUtil;
import com.patrojascontreras.config.security.jwts.services.details.UserDetailsImpl;
import com.patrojascontreras.models.dtos.request.auths.AuthenticationRequest;
import com.patrojascontreras.models.dtos.response.ErrorMessageResponse;
import com.patrojascontreras.models.dtos.response.UserInformationResponse;
import com.patrojascontreras.models.entities.Phone;
import com.patrojascontreras.models.entities.User;
import com.patrojascontreras.models.services.UsersService;

@RestController
@RequestMapping(value = "/api/authenticate")
@Tag(name = "AuthenticationController", description = "Operaciones para consultar datos de un Usuario")
public class AuthenticationController {

    private final UsersService usersService;

    private final AuthenticationManager authenticationManager;

    private final JwtTokenUtil jwtTokenUtil;

    @Autowired
    public AuthenticationController(UsersService usersService, AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil) {
        this.usersService = usersService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @Operation(summary = "Iniciar sesión de Usuario mediante Login y Password")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> generateAuthenticateToken(@RequestBody AuthenticationRequest authenticationRequest) {

        try {
            Map<String, Object> errorMap = new HashMap<String, Object>();

            LocalDateTime errorLocalDateTime;
            Integer errorStatus;
            String errorDetail = "";

            if(authenticationRequest.getEmail() == "") {
                errorLocalDateTime = LocalDateTime.now();
                errorStatus = HttpStatus.BAD_REQUEST.value();
                errorDetail = "El campo Login no puede estar vacío";

                errorMap.put("error", new ErrorMessageResponse(errorLocalDateTime, errorStatus, errorDetail));

                return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
            }

            if(authenticationRequest.getPassword() == "") {
                errorLocalDateTime = LocalDateTime.now();
                errorStatus = HttpStatus.BAD_REQUEST.value();
                errorDetail = "El campo Password no puede estar vacío";

                errorMap.put("error", new ErrorMessageResponse(errorLocalDateTime, errorStatus, errorDetail));

                return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
            }

            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(), authenticationRequest.getPassword()));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = jwtTokenUtil.generateJwtToken(authentication);
            UserDetailsImpl userBean = (UserDetailsImpl) authentication.getPrincipal();

            LocalDateTime myDateObj = LocalDateTime.now(ZoneId.of("America/Santiago"));

            DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("MMM dd, yyyy HH:mm:ss a");
            String formattedFinalDateTime = myDateObj.format(myFormatObj);

            User getUserById = usersService.getUserById(userBean.getId());

            List<Phone> phones = new ArrayList<>();
            for (Phone phoneIn : getUserById.getPhones()) {
                Phone phone = new Phone(phoneIn.getNumber(), phoneIn.getCitycode(), phoneIn.getCountrycode());
                phones.add(phone);
            }

            UserInformationResponse response = new UserInformationResponse(userBean.getId(),
                    getUserById.getCreatedAt(),
                    formattedFinalDateTime,
                    token,
                    getUserById.getIsActive(),
                    getUserById.getName(),
                    userBean.getUsername(),
                    userBean.getPassword(),
                    phones);

            return ResponseEntity.ok(response);
        } catch (BadCredentialsException e) {
            Map<String, Object> map = new HashMap<String, Object>();

            map.put("error", new ErrorMessageResponse(LocalDateTime.now(), HttpStatus.UNAUTHORIZED.value(), "Credenciales inválidas"));

            return new ResponseEntity<>(map, HttpStatus.UNAUTHORIZED);
        } catch (Exception e) {
            Map<String, Object> map = new HashMap<String, Object>();

            map.put("error", new ErrorMessageResponse(LocalDateTime.now(), HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error interno en el servidor"));

            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
