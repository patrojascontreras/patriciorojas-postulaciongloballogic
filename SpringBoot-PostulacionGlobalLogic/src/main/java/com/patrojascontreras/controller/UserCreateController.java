package com.patrojascontreras.controller;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.*;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.patrojascontreras.config.security.jwts.JwtTokenUtil;
import com.patrojascontreras.models.dtos.response.UserCreateResponse;
import com.patrojascontreras.models.dtos.response.ErrorMessageResponse;
import com.patrojascontreras.models.entities.Phone;
import com.patrojascontreras.models.entities.Role;
import com.patrojascontreras.models.entities.Roles;
import com.patrojascontreras.models.entities.User;
import com.patrojascontreras.models.services.RolesService;
import com.patrojascontreras.models.services.UsersService;
import com.patrojascontreras.validations.Validation;

@RestController
@Tag(name = "UserCreateController", description = "Operaciones para procesar datos de un Usuario")
@RequestMapping(value = "/api/admin/users")
public class UserCreateController {

    private final UsersService usersService;

    private final RolesService rolesService;

    private final JwtTokenUtil jwtTokenUtil;

    private final AuthenticationManager authenticationManager;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserCreateController(UsersService usersService, RolesService rolesService, JwtTokenUtil jwtTokenUtil, AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder) {
        this.usersService = usersService;
        this.rolesService = rolesService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
    }

    @Operation(summary = "Crear Usuario")
    @RequestMapping(value = "/sign-up", method = RequestMethod.POST)
    public ResponseEntity<Object> userInsert(@RequestBody User user) {

        try {
            Validation validation = new Validation();

            Map<String, Object> errorMap = new HashMap<String, Object>();

            LocalDateTime errorLocalDateTime;
            Integer errorStatus;
            String errorDetail = "";

            if(user.getName() == "") {
                errorLocalDateTime = LocalDateTime.now();
                errorStatus = HttpStatus.BAD_REQUEST.value();
                errorDetail = "El campo Nombre no puede estar vacío";

                errorMap.put("error", new ErrorMessageResponse(errorLocalDateTime, errorStatus, errorDetail));

                return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
            }

            if(user.getEmail() == "") {
                errorLocalDateTime = LocalDateTime.now();
                errorStatus = HttpStatus.BAD_REQUEST.value();
                errorDetail = "El campo Email no puede estar vacío";

                errorMap.put("error", new ErrorMessageResponse(errorLocalDateTime, errorStatus, errorDetail));

                return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
            }

            if(user.getPassword() == "") {
                errorLocalDateTime = LocalDateTime.now();
                errorStatus = HttpStatus.BAD_REQUEST.value();
                errorDetail = "El campo Password no puede estar vacío";

                errorMap.put("error", new ErrorMessageResponse(errorLocalDateTime, errorStatus, errorDetail));

                return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
            }

            if(!validation.validarEmail(user.getEmail())) {
                errorLocalDateTime = LocalDateTime.now();
                errorStatus = HttpStatus.BAD_REQUEST.value();
                errorDetail = "El campo Email está inválido";

                errorMap.put("error", new ErrorMessageResponse(errorLocalDateTime, errorStatus, errorDetail));

                return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
            }

            if(!validation.validarPassword(user.getPassword())) {
                errorLocalDateTime = LocalDateTime.now();
                errorStatus = HttpStatus.BAD_REQUEST.value();
                errorDetail = "El campo Contraseña está inválido";

                errorMap.put("error", new ErrorMessageResponse(errorLocalDateTime, errorStatus, errorDetail));

                return new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST);
            }

            User existByEmailLogin = usersService.existByLogin(user.getEmail());

            if(existByEmailLogin == null) {
                //Procesar creación de datos
                Set<Role> roles = new HashSet<>();

                User usrCreate = new User();

                boolean isActive = true;

                LocalDateTime myDateObj = LocalDateTime.now(ZoneId.of("America/Santiago"));

                DateTimeFormatter myFormatObj = DateTimeFormatter.ofPattern("MMM dd, yyyy HH:mm:ss a");
                String formattedFinalDateTime = myDateObj.format(myFormatObj);

                String password = passwordEncoder.encode(user.getPassword());

                usrCreate.setName(user.getName());
                usrCreate.setEmail(user.getEmail());
                usrCreate.setPassword(password);
                usrCreate.setCreatedAt(formattedFinalDateTime);
                usrCreate.setUpdatedAt(formattedFinalDateTime);
                usrCreate.setIsActive(isActive);

                String[] roleArr = null;

                if(roleArr == null) {
                    roles.add(rolesService.findByRoleName(Roles.ROLE_USER).get());
                }

                usrCreate.setRoles(roles);

                //List de Phones
                List<Phone> phones = new ArrayList<>();
                for (Phone phoneIn : user.getPhones()) {
                    // new Phone
                    Phone phone = new Phone(phoneIn.getNumber(), phoneIn.getCitycode(), phoneIn.getCountrycode());
                    // set user to Phone
                    phone.setUser(usrCreate);
                    // add phone to list
                    phones.add(phone);
                }
                //Fin List de Phones

                //Agregar listado de Phones en User
                usrCreate.setPhones(phones);
                //Fin Agregar listado de Phones en User

                usersService.userCreate(usrCreate);
                //Fin Procesar creación de datos

                //Generación de token
                Authentication authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword()));

                SecurityContextHolder.getContext().setAuthentication(authentication);
                String token = jwtTokenUtil.generateJwtToken(authentication);
                //Fin Generación de token

                UserCreateResponse response = new UserCreateResponse(usrCreate.getIdUser(),
                        usrCreate.getCreatedAt(),
                        usrCreate.getUpdatedAt(),
                        token,
                        isActive);

                return new ResponseEntity<>(response, HttpStatus.CREATED);
            } else {
                LocalDateTime localDateTime = LocalDateTime.now();
                Integer status = HttpStatus.OK.value();
                String detail = "El correo ya registrado";

                errorMap.put("error", new ErrorMessageResponse(localDateTime, status, detail));

                return new ResponseEntity<>(errorMap, HttpStatus.OK);
            }
        } catch(Exception e) {
            Map<String, Object> map = new HashMap<String, Object>();

            map.put("error", new ErrorMessageResponse(LocalDateTime.now(), HttpStatus.INTERNAL_SERVER_ERROR.value(), "Error interno en el servidor"));

            return new ResponseEntity<>(map, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}
