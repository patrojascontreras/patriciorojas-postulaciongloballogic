package com.patrojascontreras.config.security.jwts.services.impl;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.patrojascontreras.config.security.jwts.services.CustomUserDetailsService;
import com.patrojascontreras.config.security.jwts.services.details.UserDetailsImpl;
import com.patrojascontreras.models.entities.User;
import com.patrojascontreras.models.repositories.auths.LoginRepository;

@Service
public class UserDetailsServiceImpl implements CustomUserDetailsService, UserDetailsService {

    @Autowired
    private LoginRepository loginRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = loginRepository.findByEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with "
                        + "user name "+ username + " not found"));
        return UserDetailsImpl.build(user);
    }

}
