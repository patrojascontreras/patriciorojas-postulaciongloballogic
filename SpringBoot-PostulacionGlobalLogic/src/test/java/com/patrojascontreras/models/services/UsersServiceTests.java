package com.patrojascontreras.models.services;

import java.time.LocalDateTime;
import java.util.*;

import com.patrojascontreras.models.entities.Phone;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

import com.patrojascontreras.models.entities.User;
import com.patrojascontreras.models.repositories.UsersRepository;
import com.patrojascontreras.models.services.impl.UsersServiceImpl;
import com.patrojascontreras.models.services.exceptions.ObjectNotFoundException;

@SpringBootTest
public class UsersServiceTests {

    @InjectMocks
    private UsersServiceImpl usersService;

    @Mock
    private UsersRepository usersRepository;

    private Optional<User> optionalUser;

    private static final int INDEX = 0;

    //Variables para crear dato
    private static final String createRequestName = "Patricio Rojas";
    private static final String createRequestLogin = "patrojascontreras@gmail.com";
    private static final String createRequestPassword = "J@va.1234";
    private static final String requestCreatedAt = "2024-02-05 11:55 AM";
    private static final boolean createRequestIsActive = true;

    //Variables para modificar dato
    private static final String updateRequestName = "Arturo Vidal";
    private static final String updateRequestPassword = "J@va.67890";
    private static final String updateRequestUpdatedAt = "2024-02-06 12:00 PM";
    private static final boolean updateRequestIsActive = false;

    //Variable para hacer prueba en eliminar dato
    private static final String requestUserId = "A55";

    @Test
    @DisplayName("Metodo de Test para creación de un Usuario")
    public void shouldUserCreate() {
        //Given
        User usr = givenUserCreate();
        //When
        when(usersRepository.save(usr)).thenReturn(usr);
        //Then
        assertEquals(usr, usersService.userCreate(usr));
    }

    @Test
    @DisplayName("Metodo de Test para modificación de un Usuario")
    public void shouldUserUpdate() {
        //Given
        User usrCreate = givenUserCreate();

        User usrUpdate = new User();
        usrUpdate.setIdUser(usrCreate.getIdUser());
        usrUpdate.setName(updateRequestName);
        usrUpdate.setEmail(usrCreate.getEmail());
        usrUpdate.setPassword(updateRequestPassword);
        usrUpdate.setCreatedAt(usrCreate.getCreatedAt());
        usrUpdate.setUpdatedAt(updateRequestUpdatedAt);
        usrUpdate.setIsActive(updateRequestIsActive);
        //When
        when(usersRepository.findById(usrUpdate.getIdUser())).thenReturn(Optional.of(usrUpdate));
        when(usersRepository.save(usrUpdate)).thenReturn(usrUpdate);
        //Then
        assertEquals(usrUpdate, usersService.userUpdate(usrUpdate));
    }

    @Test
    @DisplayName("Metodo de Test para eliminación de un Usuario por ID")
    public void shouldUserDeleteById() {
        //Given
        User usr = givenUserCreate();
        //When
        doNothing().when(usersRepository).deleteById(usr.getIdUser());
        //Then
        usersService.deleteById(usr.getIdUser());
        verify(usersRepository, times(1)).deleteById(usr.getIdUser());
        verifyNoMoreInteractions(usersRepository);
    }

    @Test
    @DisplayName("Metodo de Test para eliminación de un Usuario por ID modo Exitoso")
    public void shouldUserDeleteByIdWithSuccess() {
        //When
        when(usersRepository.findById(anyString())).thenReturn(optionalUser);
        doNothing().when(usersRepository).deleteById(anyString());
        //Then
        usersService.deleteById(requestUserId);
        verify(usersRepository, times(1)).deleteById(anyString());
    }

    @Test
    @DisplayName("Metodo de Test para eliminación de un Usuario por ID con Objeto no encontrado")
    public void shouldUserDeleteByIdObjectNotFoundException() {
        //When
        when(usersRepository.findById(anyString())).thenThrow(new ObjectNotFoundException("Objeto no encontrado"));
        //Then
        try {
            usersService.deleteById(requestUserId);
        } catch(Exception e) {
            assertEquals(ObjectNotFoundException.class, e.getClass());
            assertEquals("Objeto no encontrado", e.getMessage());
        }
    }

    @Test
    @DisplayName("Metodo de Test para obtener el listado de todos los Usuarios")
    public void shouldFindAllUsers() {
        //Given
        User usr = givenUserCreate();
        //When
        List<User> users = Collections.singletonList(usr);
        when(usersRepository.findAll()).thenReturn(users);
        //Then
        assertEquals(users, usersService.usersListAll());
    }

    @Test
    @DisplayName("Metodo de Test para obtener un listado de todos los Usuarios")
    public void shouldFindAllThenReturnAnListOfUsers() {
        //Given
        User usr = givenUserCreate();
        //When
        List<User> listAllRepository = usersRepository.findAll();
        when(listAllRepository).thenReturn(List.of(usr));
        //Then
        List<User> response = usersService.usersListAll();

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals(User.class, response.get(INDEX).getClass());

        assertEquals(usr.getIdUser(), response.get(INDEX).getIdUser());
        assertEquals(usr.getName(), response.get(INDEX).getName());
        assertEquals(usr.getEmail(), response.get(INDEX).getEmail());
        assertEquals(usr.getPassword(), response.get(INDEX).getPassword());
        assertEquals(usr.getCreatedAt(), response.get(INDEX).getCreatedAt());
        assertEquals(usr.getUpdatedAt(), response.get(INDEX).getUpdatedAt());
        assertEquals(usr.getIsActive(), response.get(INDEX).getIsActive());
    }

    @Test
    @DisplayName("Metodo de Test para obtener el listado de todos los Usuarios por ID")
    public void shouldFindAllUsersById() {
        //Given
        User usr = givenUserCreate();
        //When
        List<User> users = Collections.singletonList(usr);
        when(usersRepository.findAllByIdUser(usr.getIdUser())).thenReturn(users);
        //Then
        assertEquals(users, usersService.usersListAllById(usr.getIdUser()));
    }

    @Test
    @DisplayName("Metodo de Test para obtener un listado de todos los Usuarios por ID")
    public void shouldFindAllThenReturnAnListOfUsersById() {
        //Given
        User usr = givenUserCreate();
        //When
        List<User> listAllRepository = usersRepository.findAllByIdUser(usr.getIdUser());
        when(listAllRepository).thenReturn(List.of(usr));
        //Then
        List<User> response = usersService.usersListAllById(usr.getIdUser());

        assertNotNull(response);
        assertEquals(1, response.size());
        assertEquals(User.class, response.get(INDEX).getClass());

        assertEquals(usr.getIdUser(), response.get(INDEX).getIdUser());
        assertEquals(usr.getName(), response.get(INDEX).getName());
        assertEquals(usr.getEmail(), response.get(INDEX).getEmail());
        assertEquals(usr.getPassword(), response.get(INDEX).getPassword());
        assertEquals(usr.getCreatedAt(), response.get(INDEX).getCreatedAt());
        assertEquals(usr.getUpdatedAt(), response.get(INDEX).getUpdatedAt());
        assertEquals(usr.getIsActive(), response.get(INDEX).getIsActive());
    }

    @Test
    @DisplayName("Metodo de Test para obtener existencia de Usuario por determinados datos")
    public void shouldUserExistInDbSuccess() {
        //Given
        User usr = givenUserCreate();

        List<User> usersList = new ArrayList<>();
        usersList.add(usr);
        //When
        when(usersRepository.findAll()).thenReturn(usersList);
        //Then
        List<User> fetchedUsers = usersService.usersListAll();
        assertThat(fetchedUsers.size()).isGreaterThan(0);
    }

    @Test
    @DisplayName("Metodo de Test para obtener un Usuario por ID")
    public void shouldFindUserById() {
        //Given
        User usr = givenUserCreate();
        //When
        when(usersRepository.findById(usr.getIdUser())).thenReturn(Optional.of(usr));
        Optional<User> returnedUsr = usersService.getUserByIdWithOptional(usr.getIdUser());
        //Then
        assertEquals(usr.getIdUser(), returnedUsr.get().getIdUser());
        verify(usersRepository).findById(usr.getIdUser());
    }

    @Test
    @DisplayName("Metodo de Test para obtener un Usuario no encontrado")
    public void shouldFindUserByIdThenReturnAnObjectNotFoundException() {
        //Given
        User usr = givenUserCreate();
        //When
        Optional<User> getUserByIdRepository = usersRepository.findById(anyString());
        when(getUserByIdRepository).thenThrow(new ObjectNotFoundException("Objeto no encontrado"));
        //Then
        try {
            usersService.getUserByIdWithOptional(usr.getIdUser());
        } catch(Exception e) {
            assertEquals(ObjectNotFoundException.class, e.getClass());
            assertEquals("Objeto no encontrado", e.getMessage());
        }
    }

    private User givenUserCreate() {
        User user = new User();
        user.setName(createRequestName);
        user.setEmail(createRequestLogin);
        user.setPassword(createRequestPassword);
        user.setCreatedAt(requestCreatedAt);
        user.setUpdatedAt(requestCreatedAt);
        user.setIsActive(createRequestIsActive);

        Phone phone01 = new Phone(87650009, 55, "02");
        Phone phone02 = new Phone(77050519, 90, "55");
        user.setPhones(List.of(phone01, phone02));

        return user;
    }

}
